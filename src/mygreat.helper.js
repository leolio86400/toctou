

const validateNumber = (value, min, max) => {
    if (typeof value !== 'number' || Number.isNaN(value)) {
        throw new Error('wrong parameter');
    }
    if (value < min || value > max) {
        throw RangeError();
    }
    return true;
};

const safeParseNumber = (value) => {
    if (typeof value === 'string' && value.length > 16) {
        // string larger than MAX_SAFE_INTEGER
        throw Error('String parameter too large');
    }
    const finalValue = Number(value);
    if (Number.isNaN(finalValue) || finalValue > Number.MAX_SAFE_INTEGER) {
        throw Error('Incorrect value');
    }
    return finalValue;
};

const getQueryParam = (req, key) => {
    if (!req || !req.query || !req.query.hasOwnProperty(key)) {
        throw Error('Missing parameter')
    }
    return req.query[key];
};

const getNumberQueryParam = (req, key) => {
    let value = getQueryParam(req, key);
    return safeParseNumber(value);
};

module.exports = {
    getQueryParam,
    getNumberQueryParam,
    validateNumber,
    safeParseNumber,
};
