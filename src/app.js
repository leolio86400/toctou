'use strict';

// requirements
const {
    getNumberQueryParam,
    validateNumber,
    safeParseNumber,
} = require("./mygreat.helper");

const express = require('express');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Send
app.get('/', (req, res) => {
    try {
        const balance = 1000;
        const amount = getNumberQueryParam(req, 'amount');
        var { newBalance, transferred } = transfer(balance, amount);
        if (newBalance !== undefined || transferred !== undefined) {
            return res.status(200).end('Successfully transferred: ' + transferred + '. Your balance: ' + newBalance);
        }
        return res.status(400).end('Insufficient funds. Your balance: ' + balance);
    } catch(err) {
        return res.status(400).end('Nope');
    }
});

// Transfer amount service
var transfer = (balance, amount) => {
    const initBalance = safeParseNumber(balance);
    const initAmount = safeParseNumber(amount);
    if (!validateNumber(initBalance, 0, Number.MAX_SAFE_INTEGER)) {
        throw Error('Invalid balance');
    }
    if (!validateNumber(initAmount, 0, Number.MAX_SAFE_INTEGER)) {
        throw Error('Invalid amount');
    }
    if (initAmount > initBalance) {
        return { undefined, undefined };
    }
    return {
        newBalance: initBalance - initAmount,
        transferred: initAmount,
    };
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, transfer };
